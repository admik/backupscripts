#! /usr/bin/env perl

if ($#ARGV != 2) {
	usage();
	exit();
}

my $osName = $^O;

if ($osName eq "freebsd") {
	$tar = "/usr/bin/tar";
} elsif ($osName eq "linux") {
	$tar = "/bin/tar";
}

$sourcePath = $ARGV[0];
$destinationPath = $ARGV[1];
$archiveName = $ARGV[2];

($sec, $min, $hour, $mday, $mon, $year) = localtime(time);

$mon++;
$year+=1900;
$mon  = "0$mon"  if $mon  < 10;
$mday = "0$mday" if $mday < 10;
$hour = "0$hour" if $hour < 10;
$min  = "0$min"  if $min  < 10;
$sec  = "0$sec"  if $sec  < 10;

$fileName = "${destinationPath}/${archiveName}_${year}_${mon}_${mday}_${hour}_${min}_${sec}_XXXX.tgz";

chdir ($sourcePath);
$cmd = "$tar czf $fileName .";
system($cmd);

exit(0);

sub usage {
print << "endprint";
Usage: archive sourcepath destinationpath archivename 
endprint
}

