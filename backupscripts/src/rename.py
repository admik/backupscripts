#! /usr/bin/env python

import os
import sys
import shutil
import datetime

def usage():
	print ("rename.py filename")
	sys.exit(2)

def main():

	if (len(sys.argv) == 1):
		usage()

	oldFileName = sys.argv[1]
	root,ext = os.path.splitext(oldFileName)

	nowDateTime = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	newFileName = root + "_" + nowDateTime + "_" + "XXXX" + ext
	
	try:
		shutil.move(oldFileName, newFileName)
	except IOError, why:
		print (why)
		
	
if __name__ == "__main__":
	main()
