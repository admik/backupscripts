#! /usr/bin/env python

import getopt
import os
import sys
import re
import time

def usage():
	print ("clean.py [-f] [-d] [-1] -c days path")
	print ("         -d debug (print instread actual remove)")
	print ("         -c skip backups for last n days")
	print ("         -1 keep backups with month day 01")
	print ("         -f force remove the last copy")
	sys.exit(2)

def main():

	try:
		opts, args = getopt.getopt(sys.argv[1:], "d1fc:")
	except getopt.GetoptError as err:
		print(err)
		usage()
		sys.exit(2)

	# default values
	debugFlag = 0
	keepFirstDays = 0
	keepDays = -1 
	filesToKeep = []
	filesToRemove = []
	lastFileTime = 0
	lastFileName = ""
	keepLastFile = 1	
	if (os.name == "posix"):
		slash = "/"
	else:
		slash = "\\"

	for o, a in opts:
		if o == "-d":
			debugFlag = 1
		if o == "-c":
			try:
				keepDays = int(a)
			except:
				print("incorrect -c value")
				usage()
		if o == "-f":
			keepLastFile = 0
		if o == "-1":
			keepFirstDays = 1

	if len(sys.argv) == 1:
		usage()

	if (keepDays == -1):
		usage()

	path = sys.argv[-1]

	try:
		files = os.listdir(path)
	except:
		print("incorrect path or permission denied")
		sys.exit(2)

	currentTime = time.time()

	for fileName in files:
		if re.search("_[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}_", fileName):

			keepFileFlag = 0
			fileNameSplit = fileName.split("_")
			year = fileNameSplit[-7]
			month = fileNameSplit[-6]
			mDay = fileNameSplit[-5]
		
			fileTimeTuple = (int(year), int(month), int(mDay), 0, 0, 0, 0, 0, 0)
			fileTime = time.mktime(fileTimeTuple)

			if (currentTime - fileTime) < (int(keepDays) * 3600 * 24):
				keepFileFlag = 1
			if (keepFirstDays == 1) and (int(mDay == "01")):
				keepFileFlag = 1

			if (lastFileTime < fileTime):
				lastFileTime = fileTime
				lastFileName = fileName

			if (keepFileFlag == 1):
				filesToKeep.append(fileName)
			else:
				res = filesToRemove.append(fileName)

	if ((len(filesToKeep) == 0) and (keepLastFile == 1) and (len(filesToRemove) != 0)):
		filesToKeep.append(lastFileName)
		res = filesToRemove.remove(lastFileName)

	if (debugFlag == 1):
		for fileName in filesToRemove:
			print ("[-]: " + fileName)

		for fileName in filesToKeep:
			print ("[+]: " + fileName)
	else:
		for fileName in filesToRemove:
			os.remove(path + slash + fileName)
	
if __name__ == "__main__":
	main()
